[![Build status](https://build.appcenter.ms/v0.1/apps/f585e204-4518-4f31-a42b-7b1a3a3ba4cc/branches/master/badge)](https://install.appcenter.ms/users/ian-sir/apps/ContinuousDeliveryWithAppCenter)

- Unit Test OK
- Sign builds OK
- Android Test OK(Test on a real device)
- Distribute builds OK

# Continuous Delivery with MS AppCenter

## Requirements

1. MS 계정
1. git 저장소
https://bitbucket.org/sirandroid/continuousdeliverywithappcenter/src/master/
```
Github, Azure, Bitbucket 중 하나만 지원
GitLab - Repository Mirroring (PULL / PUSH) 활용 가능 
```

## 주의
1. https://m.blog.naver.com/jaseazer/221317744472
1. 반드시 배포 페이지에서 다운 받아야 한다. ex) https://install.appcenter.ms/users/ian-sir/apps/continuousdeliverywithappcenter/distribution_groups/public-test
1. AppCenter 로그인은 안해도 된다. (퍼블릭으로 해도 안될 경우가 있는데, 원인은 모르겠다. 이럴 경우, 공용 테스터 하나 등록하고 로그인 시키자.)

## Steps

1. 안드로이드 프로젝트 준비 (정확한 테스트를 위해 빈 프로젝트 추천)
1. [https://appcenter.ms](https://appcenter.ms)
1. 



1. 배포 그룹을 꼭 염두하라.
- 등록된 Collaborators 만 사용 가능
- 공개하려면 Public 설정 필요


## 참고
1. unittest -> java test
1. device test -> android test
